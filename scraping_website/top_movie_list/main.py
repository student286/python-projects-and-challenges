from bs4 import BeautifulSoup
import requests

response = requests.get("https://news.ycombinator.com/news")

yc_web_page = response.text

soup = BeautifulSoup(yc_web_page, "html.parser")

articles = soup.find_all(name="span", class_="titleline")
votes = soup.find_all(name="span", class_="score")

article_texts = []
article_links = []

for article in articles:
    text = article.getText()
    article_texts.append(text)
    
    link = (article.find(name="a")).get("href")
    article_links.append(link)


article_upvotes = [int(score.getText().split(" ")[0]) for score in votes]

index_number = article_upvotes.index(max(article_upvotes))

print(article_texts[index_number])
print(article_links[index_number])
# print(article_texts)
# print(article_links)
