word = input("Enter a string \n")

# word_list = list(word)

my_dict = {}

for x in word:
    count = 0
    for i in range(0, len(word)):
        if x == word[i]:
            count+= 1
            my_dict[x] = count


print(my_dict)

max_count = 0
max_letter = ""

for i in my_dict:
    if my_dict[i] > max_count:
        max_count = my_dict[i]
        max_letter = i

print(f"maximum occuring letter is {max_letter} which is {max_count} times")


min_count = max_count
min_letter = ""

for i in my_dict:      
    if my_dict[i] < min_count:
        min_count = my_dict[i]
        min_letter = i

print(f"minimum occuring letter is {min_letter} which is {min_count} times")